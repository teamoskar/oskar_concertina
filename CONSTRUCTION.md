Concertina Construction plan
============================

The structure of Oskar Concertina essentially consists of a microcontroller (ESP32) and 8 keys. 
The replica is beginner friendly. You only have to solder (leaded) components with through-hole plating. 
You can have the board made by a board manufacturer. 
The 3D printing of the case, with 0.4 mm nozzle diameter of a Kossel 3D printer, takes about 4 hours with activated support structures. 
The assembly takes about 1 hour. 
The material costs amount to approx. € 50. 

![Concertina](images/oskar_concertina.jpg)

## Preparation
To build Oskar Concertina you need material, tools and software.
1. material ![bill of material](https://gitlab.com/teamoskar/oskar_concertina/blob/master/BOM.txt)
 * ![PCB](https://gitlab.com/teamoskar/oskar_concertina_pcb)
 * ![enclosure 3D print](https://gitlab.com/teamoskar/oskar_concertina_case)
![Material: Enclosure, LiPo battery, magnets, metal plates, screws, threaded inserts, on/off switch, LiPo battery plug, LolinD32, circuit board, buttons, resistor, pin headers
](images/material.jpg)
2. tools ![tools list](https://gitlab.com/teamoskar/oskar_concertina/blob/master/tools.txt)
 * 3D printer
 * Soldering iron and optional third hand with magnifying glass
 * side cutter
 * flat screwdriver 4.5 to 5 mm wide
 * Hexagon socket wrench 2 mm
 * Carpet knife and metal file
![Werkzeug: Screwdriver, hexagon socket wrench, soldering tin, side cutter, soldering iron, optional soldering grease and third hand with magnifier](images/tools.jpg)
3. software
 * Espressif IoT Development Framework https://github.com/espressif/esp-idf
 * ![Oskar Concertina Firmware (Oskar ESP32)](https://gitlab.com/teamoskar/oskar_esp32)

## Electronics soldering
1. First solder only the components above the microcontroller board Wemos LolinD32. Solder the resistor and only the 4 keys above the LolinD32. Attention! The solder joints of these components are then covered by the LolinD32 and can no longer be soldered after soldering the LolinD32! The 4 keys next to the LolinD32 must not be soldered to leave space for soldering the LolinD32! 
![board top, 4 keys and resistor are soldered in front of the LolinD32](images/pcb_top.jpg)
 1. Now you can solder on LolinD32. The pin headers come between the PCB and the LolinD32 with the long pins pointing down towards the LolinD32. Solder the LolinD32 pins BAT, 13, 12, 14, 27, 26, 25, 33, 32 as well as the two GND and their adjacent pins. ![2 Pin connectors are shortened with the side cutter](images/pinheader.jpg)
 2. Then solder the remaining components (LiPo plug, on/off switch and 4 keys), and you can switch from the soldering station to the computer.  
    ![Board underside with LolinD32, LiPo plug and on/off switch](images/pcb_bottom_full.jpg) 
    ![board top with 8 keys](images/pcb_top_8keys.jpg)

## Flash firmware
If you haven't installed the "Espressif IoT Development Framework" yet, follow the installation instructions of Espressif.

Download the source code of the ![Oskar ESP32 Firmware](https://gitlab.com/teamoskar/oskar_esp32)! There you will find a download button or the necessary git commands.
Establish the USB cable connection between your computer and Oskar Concertina!

Change to the downloaded (and if necessary unpacked) directory and flash the Oskar Concertina ESP32 firmware to the microcontroller with the command "make flash".
Test with "make monitor" if all keys deliver signals. Each release of a key is displayed as a bit of one byte on the monitor!

Establish a Bluetooth connection with your smartphone (or other Bluetooth enabled device that accepts Bluetooth HID keyboards)!
Oskar Concertina can only pair with a new device for a limited time (30 seconds) after switching on.
If it doesn't work immediately, disconnect Oskar Concertina from the power supply (via USB cable or LiPo battery) and repeat the pairing process within 30 seconds.
The connection to the LiPo battery is established via the on/off switch (switch on the inner position) or interrupted (switch on the outer position).

## Installation
Deburr the fresh 3D print if necessary! The screws and magnets must fit easily through the holes provided. I can recommend a carpet knife and a metal file.

Screw the threaded inserts into the holes provided in the cover. 
![Cover with threaded inserts](images/thread_insert.jpg) 
Position the board on the lid!
Place the LiPo battery protection on the key soldering points next to the LolinD32 and connect the LiPo battery to the PCB! Screw the magnets onto the two long screw sockets!
Screwing the magnets onto the two long screw sockets is a bit of a game of skill, because the strong magnets make unforeseen movements when screwing. ![Cover with platine, LiPo battery, LiPo protection and magnets](images/cover_pcb_magnets.jpg)

Position the base plate and make sure that the LiPo battery lies on the LiPo battery protector and that you do not pinch the cable of the LiPo battery! Then you can tighten the base plate.
Do not overtighten the screws. ![Oskar Concertina Rear and Hex Key](images/bottom.jpg) 

![Oskar Concertina and hexagon socket wrench](images/oskar_concertina_hexagonal_key.jpg)

## Application
To quickly attach and detach Oskar Concertina to the back of your phone, place two thin metal plates in place of the magnets between the case and the phone. You can do this easily by placing Oskar Concertina with the magnets up and the smartphone case on top. If you now place the metal plates in the smartphone case, they will find their place magnetically.  ![Oskar Concertina on Smartphone](images/smartphone_oskar_concertina.jpg) But the small keyboard is not only useful on the back of smartphones, Oskar Concertina also offers interesting possibilities as a remote control for smartphones, computers or TVs. LiPo battery can be charged via USB cable when the on/off switch is "On" (switch on the inner position).

![Braille Basic System of the German Braille](images/braille_base_system.png)

If you have not yet mastered Braille, now is the best time to learn Braille. If you're not in the mood for Braille, you can design your own keyboard layout for the 8 keys. To do this, change the entries in the table "chord_id_keycode" to oskar_esp32/main/main.c. Or you can develop a Shift mode based on the Arrow or WASD mode (arrow_modus, wasd_modus).

Have fun pressing the buttons.

## Related Links
* ![Projekt Archiv Oskar Concertina](https://gitlab.com/teamoskar/oskar_concertina)
* Website Oskar https://oskar.ddns.mobi/
* ![Erich Schmids 8 Keys Braille](https://gitlab.com/teamoskar/oskar_esp32/blob/master/brailletable.txt)
* Smartphone and Oskar Concertina Video
<html5media height="360" width="640">https://www.youtube.com/watch?v=q0peQTYuEqs</html5media>
