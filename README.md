# oskar_concertina
Oskar Concertina is a open-source, mobile braillekeyboard.
The arrangement of 8 keys in a braille cell block allows Oskar to be controlled without a supporting surface.

![Oskar Concertina](/images/oskar_concertina.jpg)

# Copyright
Copyright 2019 Johannes Strelka-Petz, johannes_at_strelka.at

# License
This documentation describes Open Hardware and is licensed under the
CERN Open Hardware Licence v1.2 or Later

You may redistribute and modify this documentation under the terms of the
CERN OHL v.1.2. (http://ohwr.org/cernohl).
This documentation is distributed
WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF
MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
PARTICULAR PURPOSE.
Please   see   the   CERN   OHL   v.1.2  for   applicable conditions

# Bill of materials
![material](/images/material.jpg)
![bill of materials](/BOM.txt)

# Tools
![tools](/images/tools.jpg)
![tools](/tools.txt)

# Contact
[https://oskar.ddns.mobi](https://oskar.ddns.mobi)
Johannes Strelka-Petz <johannes_at_oskar.ddns.mobi>