		  ____________________________________

		   OSKAR CONCERTINA BILL OF MATERIALS
		  ____________________________________


-----------------------------------------------------------------------------------
 material                                                                    count 
-----------------------------------------------------------------------------------
 Kailh low profile choc switches brown PG1350                                    8 
 Kailh low profile choc kecaps                                                   8 
 Wemos LolinD32                                                                  1 
 pin contact strips, 2.54mm pitch, x16 + x4                                      1 
 E-Switch EG1224                                                                 1 
 3,7 V 800 mAh lipo battery JST PH 2,0mm 2pin 652540                             1 
 CONN HEADER R/A 2POS 2MM, JST S2B-PH-K-S(LF)(SN), (JST PH 2.0 mm 2 pin)         1
 axial, through-hole resistor 1 kOhm, 1/4 watt                                   1 
 hex socket countersunk flat head screws M3 12mm                                 6 
 M 3 x 0,5 - 6 mm thread inserts with cutting slots                              6 
 countersunk borehole disc magnets Ø 18 mm, height 4 mm, N35, nickel-plated      2 
 PCB manufacturing                                                               1 
 Case 3D print filament                                                          1 
 - Front                                                                           
 - Back                                                                            
 - LiPo protector                                                                  
 Lead Free Solder Wire with Rosin Core Sn99 Ag0.3 Cu0.7 1mm                      1 
 USB 2.0 A-Male to Micro B Cable                                                 1 
 Metal plates (1,7" diameter)                                                    2 
 Hex L-wrench 2 mm                                                               1 
-----------------------------------------------------------------------------------
